﻿using System;
using System.Windows;

namespace RomanNumeralConverter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void BtnConverter_Click(object sender, RoutedEventArgs e)
        {
            ///////  UI Validation ( no letters , no Zero , Numbers only, no Negative Numbers  ) 
            int number;
            try
            {
                number = int.Parse(TxtRomanInt.Text);
            }
            catch (Exception)
            {
                number = 0;
            }
            if (number <= 0)
            {
                MessageBox.Show("Please Enter a valid number ...");
                TxtRomanInt.Text = ""; 
            }
            //// convert routine 
            string romanString = null;
            try
            {
                if (number > 3000)
                {
                   MessageBox.Show("out of range ... Maximum Number 3000");
                   TxtRomanInt.Text = "";
                   lblRomanString.Content = string.Empty;
                    return;
                }
                int[] roman_value_list = new int[] { 1000, 900, 500, 400,100, 90, 50, 40, 10, 9, 5, 4, 1 };
                string[] roman_char_list = new string[] { "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I" };

                for (int i = 0; i < roman_value_list.Length; i += 1)
                {
                    while (number >= roman_value_list[i])
                    {
                        number -= roman_value_list[i];
                        romanString += roman_char_list[i];
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error in Roman Conversion ... " + ex.Message.ToString());
                return;
            }
            ///// Display Roman string on the screen 
            lblRomanString.Content = romanString;
        }
        private void BtnReset_Click(object sender, RoutedEventArgs e)
        {
            lblRomanString.Content = string.Empty;
            TxtRomanInt.Text = string.Empty;
        }
        private void ExitBtn_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
